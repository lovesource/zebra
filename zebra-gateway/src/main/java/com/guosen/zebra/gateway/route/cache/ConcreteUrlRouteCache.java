package com.guosen.zebra.gateway.route.cache;

import com.guosen.zebra.gateway.route.model.RouteInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 精确 URL 到路由信息
 */
public final class ConcreteUrlRouteCache {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConcreteUrlRouteCache.class);

    private static final ConcreteUrlRouteCache INSTANCE = new ConcreteUrlRouteCache();

    /**
     * 精确 URL 到路由信息映射
     */
    private Map<String, RouteInfo> urlRouteInfoMap = new HashMap<>();

    /**
     * 微服务下包含的精确 URL 列表
     */
    private Map<String, Set<String>> microServiceUrlMap = new HashMap<>();

    private ConcreteUrlRouteCache(){}

    /**
     * 获取实例
     * @return  实例
     */
    public static ConcreteUrlRouteCache getInstance() {
        return INSTANCE;
    }

    /**
     * 更新微服务的路由信息
     * @param microServiceName  微服务名称
     * @param urlRouteInfoMapOfMicroService 此微服务的精确 URL 到路由信息的映射
     */
    public synchronized void update(String microServiceName, Map<String, RouteInfo> urlRouteInfoMapOfMicroService) {
        LOGGER.info("Begin to update micro service {} concrete url route info. count : {}",
                microServiceName,
                urlRouteInfoMapOfMicroService.keySet().size());

        // 先复制一个新的，更新完毕后再设置回去
        Map<String, RouteInfo> newUrlRouteInfoMap = new HashMap<>(urlRouteInfoMap);
        Map<String, Set<String>> newMicroServiceUrlMap = new HashMap<>(microServiceUrlMap);

        // 把原来微服务相关信息先清理掉
        Set<String> microServiceUrls = newMicroServiceUrlMap.get(microServiceName);
        if (!CollectionUtils.isEmpty(microServiceUrls)) {
            newMicroServiceUrlMap.remove(microServiceName);
            microServiceUrls.forEach(newUrlRouteInfoMap::remove);
        }

        if (!CollectionUtils.isEmpty(urlRouteInfoMapOfMicroService)) {
            newMicroServiceUrlMap.put(microServiceName, urlRouteInfoMapOfMicroService.keySet());
            newUrlRouteInfoMap.putAll(urlRouteInfoMapOfMicroService);
        }

        // 更新
        urlRouteInfoMap = newUrlRouteInfoMap;
        microServiceUrlMap = newMicroServiceUrlMap;

        LOGGER.info("Finish to update micro service {} concrete url route info.", microServiceName);
    }

    /**
     * 获取精确 URL 到路由信息的映射
     * @return 精确 URL 到路由信息的映射
     */
    public Map<String, RouteInfo> getUrlRouteInfoMap() {
        return urlRouteInfoMap;
    }

}
