package com.guosen.zebra.gateway.handler;

import com.alibaba.fastjson.JSONObject;
import com.guosen.zebra.core.exception.RpcServiceException;
import com.guosen.zebra.core.grpc.server.GenericService;
import com.guosen.zebra.gateway.exception.RouteNotFound;
import com.guosen.zebra.gateway.exception.ServiceNotFound;
import com.guosen.zebra.gateway.route.model.RouteInfo;
import com.guosen.zebra.gateway.route.router.Router;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class ZebraServiceHandlerTest {

    @Tested
    private ZebraServiceHandler zebraServiceHandler;

    @Injectable
    private Router router;

    @Injectable
    private GenericService genericService;

    @Test
    public void testRouteNotFound() {
        new Expectations() {
            {
                router.route(anyString);
                result = null;
            }
        };

        try {
            zebraServiceHandler.handleRequest("/firstService/sayHi", new JSONObject(), Collections.emptyMap());
            fail("Must have exception.");
        }
        catch (RouteNotFound e) {
            // 期望捕获到此异常
        }
        catch (ServiceNotFound e) {
            fail("Must not have this exception");
        }
    }

    @Test
    public void testServiceNotFound() {
        JSONObject requestParameter = new JSONObject();
        new Expectations() {
            {
                RouteInfo routeInfo = RouteInfo.newBuilder().build();
                router.route(anyString);
                result = routeInfo;
            }
            {
                Status status = Status.fromCode(Status.Code.UNIMPLEMENTED);
                StatusRuntimeException rootCause = new StatusRuntimeException(status);

                genericService.$invoke(anyString, anyString, anyString, anyString, requestParameter);

                result = new RpcServiceException(rootCause);
            }
        };

        try {
            zebraServiceHandler.handleRequest("/firstService/sayHi", requestParameter, Collections.emptyMap());
            fail("Must have exception.");
        }
        catch (RouteNotFound e) {
            fail("Must not have this exception");
        }
        catch (ServiceNotFound e) {

        }
    }

    @Test
    public void testOtherServiceError() {
        JSONObject requestParameter = new JSONObject();
        new Expectations() {
            {
                RouteInfo routeInfo = RouteInfo.newBuilder().build();
                router.route(anyString);
                result = routeInfo;
            }
            {
                Status status = Status.fromCode(Status.Code.INVALID_ARGUMENT);
                StatusRuntimeException rootCause = new StatusRuntimeException(status);

                genericService.$invoke(anyString, anyString, anyString, anyString, requestParameter);

                result = new RpcServiceException(rootCause);
            }
        };

        try {
            zebraServiceHandler.handleRequest("/firstService/sayHi", requestParameter, Collections.emptyMap());
            fail("Must have exception.");
        }
        catch (RouteNotFound e) {
            fail("Must not have this exception");
        }
        catch (ServiceNotFound e) {
            fail("Must not have this exception");
        }
        catch (RpcServiceException e) {
            // 期望此异常
        }
    }

    @Test
    public void testHandleSuccessfully() throws ServiceNotFound, RouteNotFound {
        JSONObject requestParameter = new JSONObject();
        new Expectations() {
            {
                RouteInfo routeInfo = RouteInfo.newBuilder().build();
                router.route(anyString);
                result = routeInfo;
            }
            {
                JSONObject resultJSON = new JSONObject();
                resultJSON.put("hello", "world");

                genericService.$invoke(anyString, anyString, anyString, anyString, requestParameter);

                result = resultJSON;
            }
        };

        JSONObject result = zebraServiceHandler.handleRequest("/firstService/sayHi", requestParameter, Collections.emptyMap());

        assertThat(result, is(notNullValue()));
        assertThat(result.getString("hello"), is("world"));
    }
}
